SUBDIRS := keys man tutorials statistics

.PHONY: default
default:
	@for SUBDIR in $(SUBDIRS); do \
		echo;\
		echo "### Making '$(TARGETNAME)' in '$${SUBDIR}'"; \
		$(MAKE) -C $${SUBDIR} $(TARGETNAME); \
	done

all: TARGETNAME=all
all: default

copy: TARGETNAME=copy
copy: default

clean: TARGETNAME=clean
clean: default

.PHONY: help
help:
	@echo "Runs one of the convenience targets in the following subdirectories:"
	@echo "    $(SUBDIRS)"
	@echo
	@echo "Targets:"
	@echo "   all    - Generate output files"
	@echo "   copy   - Copy generated files to the export directory"
	@echo "   clean  - Remove all generated files"
	@echo
	@echo "See subdirectories for details and additional targets."
