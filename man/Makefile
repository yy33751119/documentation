PO_FILES := $(wildcard *.po)
LANGS := $(PO_FILES:.po=)

authors_include_tag := \$${INKSCAPE_AUTHORS}
authors_include_tag_old := [% INCLUDE \"AUTHORS\" %]
authors_list := $$(less AUTHORS | awk -vORS=", " '{ print $$$$0 }' | sed 's/, $$$$/\n/')


.PHONY: all
all: pod get_AUTHORS


# rule to download the latest AUTHORS file from the inkscape git repository
AUTHORS: get_AUTHORS

.PHONY: get_AUTHORS
get_AUTHORS:
ifdef INKBRANCH
	wget -qN https://gitlab.com/inkscape/inkscape/raw/$(INKBRANCH)/AUTHORS -O AUTHORS.new
else
	@echo WARNING: 'INKBRANCH' is not set, downloading AUTHORS from 'master'
	wget -qN https://gitlab.com/inkscape/inkscape/raw/master/AUTHORS -O AUTHORS.new
endif
	@diff AUTHORS AUTHORS.new > /dev/null && rm AUTHORS.new || mv AUTHORS.new AUTHORS


# rules for creating .pod.in files (including .pod files for old autotools builds) and .html files
pod: $(addprefix inkscape., $(addsuffix .pod.in, $(LANGS))) AUTHORS
	@# generate .pod version of the template POD file for old autotools builds
	@sed "s/$(authors_include_tag)/$(authors_include_tag_old)/g" inkscape.pod.in > inkscape.pod

	@# generate the HTML version of the template POD file
	@pod2html --quiet --infile inkscape.pod.in --outfile inkscape-man.html --title "MAN PAGE"
	@sed -i "s/$(authors_include_tag)/$(authors_list)/g" inkscape-man.html
	
	@rm -r *.tmp

inkscape.%.pod.in: %.po AUTHORS
	po4a-translate -f pod -m inkscape.pod.in -p $*.po -l inkscape.$*.pod.in || exit 0

	@if [ -f inkscape.$*.pod.in ]; \
	then \
		# also generate .pod versions for old autotools builds \
		sed "s/$(authors_include_tag)/$(authors_include_tag_old)/g" inkscape.$*.pod.in > inkscape.$*.pod; \
		\
		# also generate the HTML versions of the translated POD files \
		pod2html --quiet --infile inkscape.$*.pod.in --outfile inkscape-man.$*.html --title "MAN PAGE"; \
		sed -i "s/$(authors_include_tag)/$(authors_list)/g" inkscape-man.$*.html; \
	fi


# rules for (re-)creating .pot and .po files
po: $(PO_FILES)

%.po: inkscape-man.pot
	msgmerge -v $*.po inkscape-man.pot > $*.po.new
	@mv -f $*.po.new $*.po

inkscape-man.pot: inkscape.pod.in
	@rm -f inkscape-man.pot
	po4a-updatepo -f pod -m inkscape.pod.in -p inkscape-man.pot
	@rm -f inkscape-man.pot~



.PHONY: copy
copy:
	mkdir -p ../export-website/man
	for file in *.html; do cp "$$file" "../export-website/man/inkscape-man$(INKVERSION)$${file#inkscape-man}"; done
	mkdir -p ../export-inkscape/man
	cp *.pod ../export-inkscape/
	cp *.pod.in ../export-inkscape/man

.PHONY: clean
clean:
	rm -f AUTHORS
	rm -f *.pod
	rm -f *.*.pod.in
	rm -f *.html

.PHONY: help
help:
	@echo "Targets:"
	@echo "   all    - Create man pages (.pod and .html files) from inkscape.pod.in"
	@echo "   po     - Update pot. and .po files for current inkscape.pod.in"
	@echo "   copy   - Copy generated HTML man pages to the export directory"
	@echo "   clean  - Remove all generated files"
