msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2010-11-11 11:37+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: interpolate-f20.svg:55(format) interpolate-f19.svg:52(format) interpolate-f18.svg:45(format) interpolate-f17.svg:45(format) interpolate-f16.svg:45(format) interpolate-f15.svg:45(format) interpolate-f14.svg:45(format) interpolate-f13.svg:45(format) interpolate-f12.svg:45(format) interpolate-f11.svg:52(format) interpolate-f10.svg:45(format) interpolate-f09.svg:45(format) interpolate-f08.svg:45(format) interpolate-f07.svg:45(format) interpolate-f05.svg:45(format) interpolate-f04.svg:47(format) interpolate-f03.svg:45(format) interpolate-f02.svg:47(format) interpolate-f01.svg:45(format)
msgid "image/svg+xml"
msgstr ""

#: interpolate-f11.svg:113(flowPara) interpolate-f04.svg:108(flowPara) interpolate-f02.svg:117(flowPara)
#, no-wrap
msgid "Exponent: 0.0"
msgstr ""

#: interpolate-f11.svg:114(flowPara) interpolate-f04.svg:109(flowPara) interpolate-f02.svg:118(flowPara)
#, no-wrap
msgid "Interpolation Steps: 6"
msgstr ""

#: interpolate-f11.svg:115(flowPara) interpolate-f04.svg:110(flowPara) interpolate-f02.svg:119(flowPara)
#, no-wrap
msgid "Interpolation Method: 2"
msgstr ""

#: interpolate-f11.svg:116(flowPara) interpolate-f04.svg:111(flowPara) interpolate-f02.svg:120(flowPara)
#, no-wrap
msgid "Duplicate Endpaths: unchecked"
msgstr ""

#: interpolate-f11.svg:117(flowPara) interpolate-f04.svg:112(flowPara) interpolate-f02.svg:121(flowPara)
#, no-wrap
msgid "Interpolate Style: unchecked"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:27(None)
msgid "@@image: 'interpolate-f01.svg'; md5=97881f947107640f7b8e333a949ff14c"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:36(None)
msgid "@@image: 'interpolate-f02.svg'; md5=e3df335f4a300dca1507f00e7ed21253"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:51(None)
msgid "@@image: 'interpolate-f03.svg'; md5=28c072ceeacdb2bdd7c2259ffbe599ad"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:60(None) tutorial-interpolate.xml:82(None)
msgid "@@image: 'interpolate-f04.svg'; md5=395c8919488dfea2833e4d14784d0eca"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:73(None)
msgid "@@image: 'interpolate-f05.svg'; md5=4b4454eb3eb522658e217d20187e32e4"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:91(None)
msgid "@@image: 'interpolate-f07.svg'; md5=0c542eea7053a06ef83d5cc38b6cfd95"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:98(None)
msgid "@@image: 'interpolate-f08.svg'; md5=e6cecf42fbd1025be356f25776f3ec8b"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:113(None)
msgid "@@image: 'interpolate-f09.svg'; md5=2c83c895aa047a9420a97089045065b4"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:122(None)
msgid "@@image: 'interpolate-f10.svg'; md5=4c6045fda8b91263faa9d0aa545a1786"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:140(None)
msgid "@@image: 'interpolate-f11.svg'; md5=d99206eb12e84db281b3d7c2b77103db"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:149(None)
msgid "@@image: 'interpolate-f12.svg'; md5=f54c78050ee73b5ac8438dd9dfb26a58"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:158(None)
msgid "@@image: 'interpolate-f13.svg'; md5=0e4c6243f3cecfe5c0c3d640c1df8433"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:167(None)
msgid "@@image: 'interpolate-f14.svg'; md5=85bacb3a295c8a8b74207f2433967ef6"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:178(None)
msgid "@@image: 'interpolate-f15.svg'; md5=f24c95652fb988c9c2d71e6028ee1ffe"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:197(None)
msgid "@@image: 'interpolate-f16.svg'; md5=b209d212b7e65ba40f49182ac54eab66"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:207(None)
msgid "@@image: 'interpolate-f17.svg'; md5=ed2c2700c8175dd6529e6cc29d43e7f8"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:216(None)
msgid "@@image: 'interpolate-f18.svg'; md5=0d4ffd793a48f69837394264b130f012"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:228(None)
msgid "@@image: 'interpolate-f19.svg'; md5=0645e0f056b2922d4f33909d9cdb12ac"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:237(None)
msgid "@@image: 'interpolate-f20.svg'; md5=0011eff8a109c5caf114ac98740ccd2d"
msgstr ""

#: tutorial-interpolate.xml:4(title)
msgid "Interpolate"
msgstr ""

#: tutorial-interpolate.xml:5(authors)
msgid "Ryan Lerch, ryanlerch at gmail dot com"
msgstr ""

#: tutorial-interpolate.xml:9(para)
msgid "This document explains how to use Inkscape's Interpolate extension"
msgstr ""

#: tutorial-interpolate.xml:14(title)
msgid "Introduction"
msgstr ""

#: tutorial-interpolate.xml:15(para)
msgid "Interpolate does a <firstterm>linear interpolation</firstterm> between two or more selected paths. It basically means that it “fills in the gaps” between the paths and transforms them according to the number of steps given."
msgstr ""

#: tutorial-interpolate.xml:16(para)
msgid "To use the Interpolate extension, select the paths that you wish to transform, and choose <command>Extensions &gt; Generate From Path &gt; Interpolate</command> from the menu."
msgstr ""

#: tutorial-interpolate.xml:17(para)
msgid "Before invoking the extension, the objects that you are going to transform need to be <emphasis>paths</emphasis>. This is done by selecting the object and using <command>Path &gt; Object to Path</command> or <keycap>Shift+Ctrl+C</keycap>. If your objects are not paths, the extension will do nothing."
msgstr ""

#: tutorial-interpolate.xml:21(title)
msgid "Interpolation between two identical paths"
msgstr ""

#: tutorial-interpolate.xml:22(para)
msgid "The simplest use of the Interpolate extension is to interpolate between two paths that are identical. When the extension is called, the result is that the space between the two paths is filled with duplicates of the original paths. The number of steps defines how many of these duplicates are placed."
msgstr ""

#: tutorial-interpolate.xml:23(para) tutorial-interpolate.xml:47(para)
msgid "For example, take the following two paths:"
msgstr ""

#: tutorial-interpolate.xml:32(para)
msgid "Now, select the two paths, and run the Interpolate extension with the settings shown in the following image."
msgstr ""

#: tutorial-interpolate.xml:41(para)
msgid "As can be seen from the above result, the space between the two circle-shaped paths has been filled with 6 (the number of interpolation steps) other circle-shaped paths. Also note that the extension groups these shapes together."
msgstr ""

#: tutorial-interpolate.xml:45(title)
msgid "Interpolation between two different paths"
msgstr ""

#: tutorial-interpolate.xml:46(para)
msgid "When interpolation is done on two different paths, the program interpolates the shape of the path from one into the other. The result is that you get a morphing sequence between the paths, with the regularity still defined by the Interpolation Steps value."
msgstr ""

#: tutorial-interpolate.xml:56(para)
msgid "Now, select the two paths, and run the Interpolate extension. The result should be like this:"
msgstr ""

#: tutorial-interpolate.xml:65(para)
msgid "As can be seen from the above result, the space between the circle-shaped path and the triangle-shaped path has been filled with 6 paths that progress in shape from one path to the other."
msgstr ""

#: tutorial-interpolate.xml:67(para)
msgid "When using the Interpolate extension on two different paths, the <emphasis>position</emphasis> of the starting node of each path is important. To find the starting node of a path, select the path, then choose the Node Tool so that the nodes appear and press <keycap>TAB</keycap>. The first node that is selected is the starting node of that path."
msgstr ""

#: tutorial-interpolate.xml:69(para)
msgid "See the image below, which is identical to the previous example, apart from the node points being displayed. The node that is green on each path is the starting node."
msgstr ""

#: tutorial-interpolate.xml:78(para)
msgid "The previous example (shown again below) was done with these nodes being the starting node."
msgstr ""

#: tutorial-interpolate.xml:87(para)
msgid "Now, notice the changes in the interpolation result when the triangle path is mirrored so the starting node is in a different position:"
msgstr ""

#: tutorial-interpolate.xml:106(title)
msgid "Interpolation Method"
msgstr ""

#: tutorial-interpolate.xml:107(para)
msgid "One of the parameters of the Interpolate extension is the Interpolation Method. There are 2 interpolation methods implemented, and they differ in the way that they calculate the curves of the new shapes. The choices are either Interpolation Method 1 or 2."
msgstr ""

#: tutorial-interpolate.xml:109(para)
msgid "In the examples above, we used Interpolation Method 2, and the result was:"
msgstr ""

#: tutorial-interpolate.xml:118(para)
msgid "Now compare this to Interpolation Method 1:"
msgstr ""

#: tutorial-interpolate.xml:127(para)
msgid "The differences in how these methods calculate the numbers is beyond the scope of this document, so simply try both, and use which ever one gives the result closest to what you intend."
msgstr ""

#: tutorial-interpolate.xml:132(title)
msgid "Exponent"
msgstr ""

#: tutorial-interpolate.xml:134(para)
msgid "The <firstterm>exponent parameter</firstterm> controls the spacing between steps of the interpolation. An exponent of 0 makes the spacing between the copies all even."
msgstr ""

#: tutorial-interpolate.xml:136(para)
msgid "Here is the result of another basic example with an exponent of 0."
msgstr ""

#: tutorial-interpolate.xml:145(para)
msgid "The same example with an exponent of 1:"
msgstr ""

#: tutorial-interpolate.xml:154(para)
msgid "with an exponent of 2:"
msgstr ""

#: tutorial-interpolate.xml:163(para)
msgid "and with an exponent of -1:"
msgstr ""

#: tutorial-interpolate.xml:172(para)
msgid "When dealing with exponents in the Interpolate extension, the <emphasis>order</emphasis> that you select the objects is important. In the examples above, the star-shaped path on the left was selected first, and the hexagon-shaped path on the right was selected second."
msgstr ""

#: tutorial-interpolate.xml:174(para)
msgid "View the result when the path on the right was selected first. The exponent in this example was set to 1:"
msgstr ""

#: tutorial-interpolate.xml:185(title)
msgid "Duplicate Endpaths"
msgstr ""

#: tutorial-interpolate.xml:186(para)
msgid "This parameter defines whether the group of paths that is generated by the extension <emphasis>includes a copy</emphasis> of the original paths that interpolate was applied on."
msgstr ""

#: tutorial-interpolate.xml:190(title)
msgid "Interpolate Style"
msgstr ""

#: tutorial-interpolate.xml:191(para)
msgid "This parameter is one of the neat functions of the interpolate extension. It tells the extension to attempt to change the style of the paths at each step. So if the start and end paths are different colors, the paths that are generated will incrementally change as well."
msgstr ""

#: tutorial-interpolate.xml:193(para)
msgid "Here is an example where the Interpolate Style function is used on the fill of a path:"
msgstr ""

#: tutorial-interpolate.xml:203(para)
msgid "Interpolate Style also affects the stroke of a path:"
msgstr ""

#: tutorial-interpolate.xml:212(para)
msgid "Of course, the path of the start point and the end point does not have to be the same either:"
msgstr ""

#: tutorial-interpolate.xml:223(title)
msgid "Using Interpolate to fake irregular-shaped gradients"
msgstr ""

#: tutorial-interpolate.xml:225(para)
msgid "It is not possible in Inkscape (yet) to create a gradient other than linear (straight line) or radial (round). However, it can be faked using the Interpolate extension and Interpolate Style. A simple example follows — draw two lines of different strokes:"
msgstr ""

#: tutorial-interpolate.xml:233(para)
msgid "And interpolate between the two lines to create your gradient:"
msgstr ""

#: tutorial-interpolate.xml:245(title)
msgid "Conclusion"
msgstr ""

#: tutorial-interpolate.xml:246(para)
msgid "As demonstrated above, the Inkscape Interpolate extension is a powerful tool. This tutorial covers the basics of this extension, but experimentation is the key to exploring interpolation further."
msgstr ""

#. Put one translator per line, in the form of NAME <EMAIL>, YEAR1, YEAR2
#: tutorial-interpolate.xml:0(None)
msgid "translator-credits"
msgstr ""

