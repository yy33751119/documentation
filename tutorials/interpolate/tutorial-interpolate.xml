<book>

<chapter id="INTERPOLATE">
<title>Interpolate</title>
<author>Ryan Lerch, ryanlerch at gmail dot com</author>
<!-- Copyright (C) 2007 ryan lerch - ryanlerch at gmail dot com -->

<abstract>
<para>This document explains how to use Inkscape's Interpolate extension</para>
</abstract>


<sect1>
<title>Introduction</title>
<para>Interpolate does a <firstterm>linear interpolation</firstterm> between two or more selected paths. It basically means that it “fills in the gaps” between the paths and transforms them according to the number of steps given.</para>
<para>To use the Interpolate extension, select the paths that you wish to transform, and choose <command>Extensions > Generate From Path > Interpolate</command> from the menu.</para>
<para>Before invoking the extension, the objects that you are going to transform need to be <emphasis>paths</emphasis>. This is done by selecting the object and using <command>Path > Object to Path</command> or <keycap>Shift+Ctrl+C</keycap>. If your objects are not paths, the extension will do nothing.</para>
</sect1>

<sect1>
<title>Interpolation between two identical paths</title>
<para>The simplest use of the Interpolate extension is to interpolate between two paths that are identical. When the extension is called, the result is that the space between the two paths is filled with duplicates of the original paths. The number of steps defines how many of these duplicates are placed.</para>
<para>For example, take the following two paths:</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="interpolate-f01.svg"/>
</imageobject>
</mediaobject>
</figure>

<para>Now, select the two paths, and run the Interpolate extension with the settings shown in the following image.</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="interpolate-f02.svg"/>
</imageobject>
</mediaobject>
</figure>

<para>As can be seen from the above result, the space between the two circle-shaped paths has been filled with 6 (the number of interpolation steps) other circle-shaped paths. Also note that the extension groups these shapes together.</para>
</sect1>

<sect1>
<title>Interpolation between two different paths</title> 
<para>When interpolation is done on two different paths, the program interpolates the shape of the path from one into the other. The result is that you get a morphing sequence between the paths, with the regularity still defined by the Interpolation Steps value.</para>
<para>For example, take the following two paths:</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="interpolate-f03.svg"/>
</imageobject>
</mediaobject>
</figure>

<para>Now, select the two paths, and run the Interpolate extension. The result should be like this:</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="interpolate-f04.svg"/>
</imageobject>
</mediaobject>
</figure>

<para>As can be seen from the above result, the space between the circle-shaped path and the triangle-shaped path has been filled with 6 paths that progress in shape from one path to the other.</para>

<para>When using the Interpolate extension on two different paths, the <emphasis>position</emphasis> of the starting node of each path is important. To find the starting node of a path, select the path, then choose the Node Tool so that the nodes appear and press <keycap>TAB</keycap>. The first node that is selected is the starting node of that path.</para>

<para>See the image below, which is identical to the previous example, apart from the node points being displayed. The node that is green on each path is the starting node.</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="interpolate-f05.svg"/>
</imageobject>
</mediaobject>
</figure>

<para>The previous example (shown again below) was done with these nodes being the starting node.</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="interpolate-f04.svg"/>
</imageobject>
</mediaobject>
</figure>

<para>Now, notice the changes in the interpolation result when the triangle path is mirrored so the starting node is in a different position:</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="interpolate-f07.svg"/>
</imageobject>
</mediaobject>
</figure>

<figure>
<mediaobject>
<imageobject><imagedata fileref="interpolate-f08.svg"/>
</imageobject>
</mediaobject>
</figure>

</sect1>

<sect1>
<title>Interpolation Method</title>
<para>One of the parameters of the Interpolate extension is the Interpolation Method. There are 2 interpolation methods implemented, and they differ in the way that they calculate the curves of the new shapes. The choices are either Interpolation Method 1 or 2.</para>

<para>In the examples above, we used Interpolation Method 2, and the result was:</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="interpolate-f09.svg"/>
</imageobject>
</mediaobject>
</figure>

<para>Now compare this to Interpolation Method 1:</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="interpolate-f10.svg"/>
</imageobject>
</mediaobject>
</figure>

<para>The differences in how these methods calculate the numbers is beyond the scope of this document, so simply try both, and use which ever one gives the result closest to what you intend.</para>

</sect1>

<sect1>
<title>Exponent</title>

<para>The <firstterm>exponent parameter</firstterm> controls the spacing between steps of the interpolation. An exponent of 0 makes the spacing between the copies all even.</para>

<para>Here is the result of another basic example with an exponent of 0.</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="interpolate-f11.svg"/>
</imageobject>
</mediaobject>
</figure>

<para>The same example with an exponent of 1:</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="interpolate-f12.svg"/>
</imageobject>
</mediaobject>
</figure>

<para>with an exponent of 2:</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="interpolate-f13.svg"/>
</imageobject>
</mediaobject>
</figure>

<para>and with an exponent of -1:</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="interpolate-f14.svg"/>
</imageobject>
</mediaobject>
</figure>

<para>When dealing with exponents in the Interpolate extension, the <emphasis>order</emphasis> that you select the objects is important. In the examples above, the star-shaped path on the left was selected first, and the hexagon-shaped path on the right was selected second.</para>

<para>View the result when the path on the right was selected first. The exponent in this example was set to 1:</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="interpolate-f15.svg"/>
</imageobject>
</mediaobject>
</figure>
</sect1>

<sect1>
<title>Duplicate Endpaths</title>
<para>This parameter defines whether the group of paths that is generated by the extension <emphasis>includes a copy</emphasis> of the original paths that interpolate was applied on.</para>
</sect1>

<sect1>
<title>Interpolate Style</title>
<para>This parameter is one of the neat functions of the interpolate extension. It tells the extension to attempt to change the style of the paths at each step. So if the start and end paths are different colors, the paths that are generated will incrementally change as well.</para>

<para>Here is an example where the Interpolate Style function is used on the fill of a path:</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="interpolate-f16.svg"/>
</imageobject>
</mediaobject>
</figure>


<para>Interpolate Style also affects the stroke of a path:</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="interpolate-f17.svg"/>
</imageobject>
</mediaobject>
</figure>

<para>Of course, the path of the start point and the end point does not have to be the same either:</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="interpolate-f18.svg"/>
</imageobject>
</mediaobject>
</figure>
</sect1>

<sect1>
<title>Using Interpolate to fake irregular-shaped gradients</title>

<para>At the time when gradient meshes were not implemented in Inkscape, it was not possible to create a gradient other than linear (straight line) or radial (round). However, it could be faked using the Interpolate extension and Interpolate Style. A simple example follows — draw two lines of different strokes:</para>
<figure>
<mediaobject>
<imageobject><imagedata fileref="interpolate-f19.svg"/>
</imageobject>
</mediaobject>
</figure>

<para>And interpolate between the two lines to create your gradient:</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="interpolate-f20.svg"/>
</imageobject>
</mediaobject>
</figure>

</sect1>

<sect1>
<title>Conclusion</title>
<para>As demonstrated above, the Inkscape Interpolate extension is a powerful tool. This tutorial covers the basics of this extension, but experimentation is the key to exploring interpolation further.</para>
</sect1>

</chapter>
</book>
