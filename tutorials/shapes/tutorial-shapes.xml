<book>

<chapter id="SHAPES">
<title>Shapes</title>
<author>bulia byak, buliabyak@users.sf.net</author>
<!-- Copyright (C) 2004-5 bulia byak, buliabyak@users.sf.net -->

<abstract>
<para>
This tutorial covers the four shape tools: Rectangle, Ellipse, Star, and Spiral.
We will demonstrate the capabilities of Inkscape shapes and show
examples of how and when they could be used.
</para>

<para>
Use <keycap>Ctrl+Arrows</keycap>, <keycap>mousewheel</keycap>, or <keycap>middle button 
drag</keycap> to scroll the page down. 
For basics of object creation, selection, and transformation, see the Basic 
tutorial in <command>Help > Tutorials</command>.
</para>
</abstract>

<sect1>
<para>
Inkscape has four versatile <firstterm>shape tools</firstterm>, each tool capable of
creating and editing its own type of shapes. A shape is an object which you can modify
in ways unique to this shape type, using draggable <firstterm>handles</firstterm> and
numeric <firstterm>parameters</firstterm> that determine the shape's appearance.
</para>

<para>
For example, with a star you can alter the number of tips, their length, angle,
rounding, etc. &#8212; but a star remains a star. A shape is “less free” than a simple
path, but it's often more interesting and useful. You can always convert a shape
to a path (<keycap>Shift+Ctrl+C</keycap>), but the reverse conversion is not possible.
</para>

<para>
The shape tools are <firstterm>Rectangle</firstterm>, <firstterm>Ellipse</firstterm>,
<firstterm>Star</firstterm>, and <firstterm>Spiral</firstterm>. First, let's look at how
shape tools work in general; then we'll explore each shape type in detail.
</para>
</sect1>

<sect1>
<title>General tips</title>
<para>
A new shape is created by <keycap>drag</keycap>ging on canvas with the 
corresponding tool. Once the shape is created (and so long as it is 
selected), it displays its handles as white diamond, square or round
marks (depending on the tools), so you can immediately edit what you
created by dragging these handles. 
</para>

<para>
All four kinds of shapes display their handles in all four shape tools as well as in the
Node tool (<keycap>F2</keycap>). When you hover your mouse over a handle, it tells you
in the statusbar what this handle will do when dragged or clicked with different modifiers.
</para>

<para>
Also, each shape tool displays its parameters in the <firstterm>Tool Controls
bar</firstterm> (which runs horizontally above the canvas). Usually it has a few numeric
entry fields and a button to reset the values to defaults. When shape(s) of the current
tool's native type are selected, editing the values in the Controls bar changes the
selected shape(s).</para>  

<para>
Any changes made to the Tool Controls are remembered and used for the next object you
draw with that tool. For example, after you change the number of tips of a star, new
stars will have this number of tips as well when drawn. Moreover, even simply selecting
a shape sends its parameters to the Tool Controls bar and thus sets the values for newly
created shapes of this type.
</para>

<para>When in a shape tool, selecting an object can be done by <keycap>click</keycap>ing
on it. <keycap>Ctrl+click</keycap> (select in group) and <keycap>Alt+click</keycap>
(select under) also work as they do in Selector tool. <keycap>Esc</keycap> deselects.
</para>
</sect1>

<sect1>
<title>Rectangles</title>
<para>
A <firstterm>rectangle</firstterm> is the simplest but perhaps the most common shape in 
design and illustration. Inkscape attempts to make creating and
editing rectangles as easy and convenient as possible.
</para>

<para>
Switch to the Rectangle tool by <keycap>F4</keycap> or by clicking its toolbar button.
Draw a new rectangle alongside this blue one:
</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="shapes-f01.svg"/>
</imageobject>
</mediaobject>
</figure>

<para>
Then, without leaving the Rectangle tool, switch selection from one
rectangle to the other by clicking on them.
</para>

<para>
Rectangle drawing shortcuts:
</para>
<itemizedlist>
<listitem><para>
With <keycap>Ctrl</keycap>, draw a square or an integer-ratio (2:1, 3:1, etc) rectangle.
</para></listitem>
<listitem><para>
With <keycap>Shift</keycap>, draw around the starting point as center.
</para></listitem>
</itemizedlist>

<para>
As you see, the selected rectangle (the just-drawn rectangle is always selected) shows
three handles in three of its corners. In fact, these are four handles, but two of them
(in the top right corner) overlap if the rectangle is not rounded. These two are the
<firstterm>rounding handles</firstterm>; the other two (top left and bottom right) are
<firstterm>resize handles</firstterm>.
</para>

<para>
Let's look at the rounding handles first. Grab one of them and drag down. All four
corners of the rectangle become rounded, and you can now see the second rounding handle
&#8212; it stays in the original position in the corner. If you want circular rounded
corners, that is all you need to do. If you want corners which are rounded farther along
one side than along the other, you can move that other handle leftwards.
</para>

<para>
Here, the first two rectangles have circular rounded corners, and the other two have
elliptic rounded corners:
</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="shapes-f02.svg"/>
</imageobject>
</mediaobject>
</figure>

<para>
Still in the Rectangle tool, click on these rectangles to select, and 
observe their rounding handles.
</para>

<para>
Often, the radius and shape of the rounded corners must be constant within the entire
composition, even if the sizes of the rectangles are different (think diagrams with
rounded boxes of various sizes). Inkscape makes this easy. Switch to the Selector tool;
in its Tool Controls bar, there's a group of four toggle buttons, the second from the
left showing two concentric rounded corners. This is how you control whether the
rounded corners are scaled when the rectangle is scaled or not.
</para>

<para>
For example, here the original red rectangle is duplicated and scaled several times, up
and down, to different proportions, with the “Scale rounded corners” button
<emphasis>off</emphasis>:
</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="shapes-f03.svg"/>
</imageobject>
</mediaobject>
</figure>

<para>
Note how the size and shape of the rounded corners is the same
in all rectangles, so that the roundings align exactly in the top 
right corner where they all meet. All the dotted blue rectangles are 
obtained from the original red rectangle just by scaling in Selector, 
without any manual readjustment of the rounding handles.
</para>

<para>
For a comparison, here is the same composition but now created 
with the “Scale rounded corners” button <emphasis>on</emphasis>:
</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="shapes-f04.svg"/>
</imageobject>
</mediaobject>
</figure>

<para>
Now the rounded corners are as different as the rectangles they belong to, and there
isn't a slightest agreement in the top right corner (zoom in to see). This is the same
(visible) result as you would get by converting the original rectangle to a path
(<keycap>Ctrl+Shift+C</keycap>) and scaling it as path.
</para>

<para>
Here are the shortcuts for the rounding handles of a rectangle:
</para>
<itemizedlist>
<listitem><para>
Drag with <keycap>Ctrl</keycap> to make the other radius the same (circular rounding).
</para></listitem>
<listitem><para>
<keycap>Ctrl+click</keycap> to make the other radius the same without dragging.
</para></listitem>

<listitem><para>
<keycap>Shift+click</keycap> to remove rounding.
</para></listitem>
</itemizedlist>

<para>
You may have noticed that the Rectangle tool's Controls bar shows the horizontal
(<firstterm>Rx</firstterm>) and vertical (<firstterm>Ry</firstterm>) rounding radii for
the selected rectangle and lets you set them precisely using any length units. The
<command>Not rounded</command> button does what is says &#8212; removes rounding from
the selected rectangle(s).</para>

<para>An important advantage of these controls is that they can affect many rectangles
at once. For example, if you want to change all rectangles in the layer, just do
<keycap>Ctrl+A</keycap> (<command>Select All</command>) and set the parameters you need
in the Controls bar. If any non-rectangles are selected, they will be ignored &#8212; only
rectangles will be changed.
</para>

<para>
Now let's look at the resize handles of a rectangle. You might wonder,
why do we need them at all, if we can just as well resize the rectangle
with Selector? 
</para>

<para>
The problem with Selector is that its notion of horizontal and vertical is always that
of the document page. By contrast, a rectangle's resize handles scale it <emphasis>along
that rectangle's sides</emphasis>, even if the rectangle is rotated or skewed. For
example, try to resize this rectangle first with Selector and then with its resize
handles in Rectangle tool:
</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="shapes-f05.svg"/>
</imageobject>
</mediaobject>
</figure>

<para>
Since the resize handles are two, you can resize the rectangle into 
any direction or even move it along its sides. Resize handles 
always preserve the rounding radii.
</para>

<para>
Here are the shortcuts for the resize handles:
</para>

<itemizedlist>
<listitem><para> Drag with <keycap>Ctrl</keycap> to snap to the sides or the diagonal of
the rectangle.  In other words, <keycap>Ctrl</keycap> preserves either width, or height,
or the width/height ratio of the rectangle (again, in its own coordinate system which
may be rotated or skewed).  </para></listitem>
</itemizedlist>

<para>
Here is the same rectangle, with the gray dotted lines showing the 
directions to which the resize handles stick when dragged with 
<keycap>Ctrl</keycap> (try it):
</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="shapes-f06.svg"/>
</imageobject>
</mediaobject>
</figure>

<para>
By slanting and rotating a rectangle, then duplicating it and resizing with its resize
handles, 3D compositions can be created easily:
</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="shapes-f07.svg"/>
</imageobject>
</mediaobject>
</figure>

<para>
Here are some more examples of rectangle compositions, including
rounding and gradient fills:
</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="shapes-f08.svg"/>
</imageobject>
</mediaobject>
</figure>

</sect1>

<sect1>
<title>Ellipses</title>
<para>
The Ellipse tool (<keycap>F5</keycap>) can create ellipses and circles, which you can 
turn into segments or arcs. The drawing shortcuts are the same as 
those of the rectangle tool:
</para>

<itemizedlist>
<listitem><para>
With <keycap>Ctrl</keycap>, draw a circle or an integer-ratio (2:1, 3:1, etc.) ellipse.
</para></listitem>
<listitem><para>
With <keycap>Shift</keycap>, draw around the starting point as center.
</para></listitem>
</itemizedlist>

<para>
Let's explore the handles of an ellipse. Select this one:
</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="shapes-f09.svg"/>
</imageobject>
</mediaobject>
</figure>

<para>
Once again, you see three handles initially, but in fact they are four.  The rightmost
handle is two overlapping handles that let you “open” the ellipse. Drag that rightmost
handle, then drag the other handle which becomes visible under it, to get a variety
of pie-chart segments or arcs:
</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="shapes-f10.svg"/>
</imageobject>
</mediaobject>
</figure>

<para>
To get a <firstterm>segment</firstterm> (an arc plus two radii), drag
<emphasis>outside</emphasis> the ellipse; to get an <firstterm>arc</firstterm>, drag
<emphasis>inside</emphasis> it. Above, there are 4 segments on the left and 3 arcs on
the right. Note that arcs are unclosed shapes, i.e. the stroke only goes along the
ellipse but does not connect the ends of the arc. You can make this obvious if you
remove the fill, leaving only stroke:
</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="shapes-f11.svg"/>
</imageobject>
</mediaobject>
</figure>

<para>
Note the fan-like group of narrow segments on the left. It was easy
to create using <firstterm>angle snapping</firstterm> of the handle with <keycap>Ctrl</keycap>. Here are the 
arc/segment handle shortcuts:
</para>

<itemizedlist>
<listitem><para>
With <keycap>Ctrl</keycap>, snap the handle every 15 degrees when dragging.
</para></listitem>
<listitem><para>
<keycap>Shift+click</keycap> to make the ellipse whole (not arc or segment).
</para></listitem>
</itemizedlist>

<para>
The snap angle can be changed in Inkscape Preferences (in <command>Behavior &gt; Steps</command>). 
</para>

<para>
The other two handles of the ellipse are used for resizing it around its center. Their
shortcuts are similar to those of the rounding handles of a rectangle:
</para>

<itemizedlist>
<listitem><para>
Drag with <keycap>Ctrl</keycap> to make a circle (make the other radius the same).
</para></listitem>
<listitem><para>
<keycap>Ctrl+click</keycap> to make a circle without dragging.
</para></listitem>
</itemizedlist>

<para>
And, like the rectangle resize handles, these ellipse handles adjust the height and
width of the ellipse in <emphasis>the ellipse's own coordinates</emphasis>.  This means
that a rotated or skewed ellipse can easily be stretched or squeezed along its original
axes while remaining rotated or skewed.  Try to resize any of these ellipses by their
resize handles:
</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="shapes-f12.svg"/>
</imageobject>
</mediaobject>
</figure>

</sect1>

<sect1>
<title>Stars</title>
<para>
Stars are the most complex and the most exciting Inkscape shape. If you want to wow your
friends by Inkscape, let them play with the Star tool. It's endlessly entertaining
&#8212; outright addictive!
</para>

<para>
The Star tool can create two similar but distinct kinds of objects: stars and
polygons. A star has two handles whose positions define the length and shape of its
tips; a polygon has just one handle which simply rotates and resizes the polygon when
dragged:
</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="shapes-f13.svg"/>
</imageobject>
</mediaobject>
</figure>

<para>
In the Controls bar of the Star tool, the first two buttons control how the shape is 
drawn (regular polygon or star). Next, a numeric field sets the <firstterm>number of
vertices</firstterm> of a star or polygon. This parameter is only editable via the
Controls bar. The allowed range is from 3 (obviously) to 1024, but you shouldn't try
large numbers (say, over 200) if your computer is slow.
</para>

<para>
When drawing a new star or polygon,
</para>

<itemizedlist>
<listitem><para>
Drag with <keycap>Ctrl</keycap> to snap the angle to 15 degree increments.
</para></listitem>
</itemizedlist>

<para>
Naturally, a star is a much more interesting shape (though polygons are often more
useful in practice). The two handles of a star have slightly different functions. The
first handle (initially it is on a vertex, i.e. on a <emphasis>convex</emphasis> corner
of the star) makes the star rays longer or shorter, but when you rotate it (relative to
the center of the shape), the other handle rotates accordingly.  This means you cannot
skew the star's rays with this handle.
</para>

<para>
The other handle (initially in a <emphasis>concave</emphasis> corner between two
vertices) is, conversely, free to move both radially and tangentially, without affecting
the vertex handle. (In fact, this handle can itself become vertex by moving farther from
the center than the other handle.) This is the handle that can skew the star's tips to
get all sorts of crystals, mandalas, snowflakes, and porcupines:
</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="shapes-f14.svg"/>
</imageobject>
</mediaobject>
</figure>

<para>
If you want just a plain regular star without any such lacework, you 
can make the skewing handle behave as the non-skewing one:
</para>

<itemizedlist>
<listitem><para>
Drag with <keycap>Ctrl</keycap> to keep the star rays strictly radial (no skew).
</para></listitem>
<listitem><para>
<keycap>Ctrl+click</keycap> to remove the skew without dragging.
</para></listitem>
</itemizedlist>

<para>
As a useful complement for the on-canvas handle dragging, the Controls bar has the
<command>Spoke ratio</command> field which defines the ratio of the two handles'
distances to the center.
</para>

<para>
Inkscape stars have two more tricks in their bag. In geometry, a polygon is a shape with
straight line edges and sharp corners. In the real world, however, various degrees of
curvilinearity and roundedness are normally present &#8212; and Inkscape can do that
too.  Rounding a star or polygon works a bit differently from rounding a rectangle,
however. You don't use a dedicated handle for this, but
</para>

<itemizedlist>
<listitem><para>
<keycap>Shift+drag</keycap> a handle tangentially to round the star or polygon.
</para></listitem>
<listitem><para>
<keycap>Shift+click</keycap> a handle to remove rounding.
</para></listitem>
</itemizedlist>

<para>
“Tangentially” means in a direction perpendicular to the direction to the center. If you
“rotate” a handle with Shift counterclockwise around the center, you get positive
roundedness; with clockwise rotation, you get negative roundedness. (See below for
examples of negative roundedness.)
</para>

<para>
Here's a comparison of a rounded square (Rectangle tool) with a rounded 4-sided polygon
(Star tool):
</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="shapes-f15.svg"/>
</imageobject>
</mediaobject>
</figure>

<para>
As you can see, while a rounded rectangle has straight line segments in its sides and
circular (generally, elliptic) roundings, a rounded polygon or star has no straight
lines at all; its curvature varies smoothly from the maximum (in the corners) to the
minimum (mid-way between the corners). Inkscape does this simply by adding collinear
Bezier tangents to each node of the shape (you can see them if you convert the shape to
path and examine it in Node tool).
</para>

<para>
The <command>Rounded</command> parameter which you can adjust in the Controls bar is the
ratio of the length of these tangents to the length of the polygon/star sides to which
they are adjacent. This parameter can be negative, which reverses the direction of
tangents. The values of about 0.2 to 0.4 give “normal” rounding of the kind you would
expect; other values tend to produce beautiful, intricate, and totally unpredictable
patterns. A star with a large roundedness value may reach far beyond the positions of
its handles. Here are a few examples, each indicating its roundedness value:
</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="shapes-f16.svg"/>
</imageobject>
</mediaobject>
</figure>

<para>
If you want the tips of a star to be sharp but the concaves smooth or vice versa, this
is easy to do by creating an <firstterm>offset</firstterm> (<keycap>Ctrl+J</keycap>)
from the star:
</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="shapes-f17.svg"/>
</imageobject>
</mediaobject>
</figure>

<para>
<keycap>Shift+drag</keycap>ging star handles in Inkscape is one of the finest pursuits
known to man. But it can get better still.
</para>

<para>
To closer imitate real world shapes, Inkscape can <firstterm>randomize</firstterm> (i.e.
randomly distort) its stars and polygons. Slight randomization makes a star less
regular, more humane, often funny; strong randomization is an exciting way to obtain a
variety of crazily unpredictable shapes. A rounded star remains smoothly rounded when
randomized. Here are the shortcuts:
</para>

<itemizedlist>
<listitem><para>
<keycap>Alt+drag</keycap> a handle tangentially to randomize the star or polygon.
</para></listitem>
<listitem><para>
<keycap>Alt+click</keycap> a handle to remove randomization.
</para></listitem>
</itemizedlist>

<para>
As you draw or handle-drag-edit a randomized star, it will “tremble” because each unique
position of its handles corresponds to its own unique randomization. So, moving a handle
without Alt re-randomizes the shape at the same randomization level, while Alt-dragging
it keeps the same randomization but adjusts its level. Here are stars whose parameters
are exactly the same, but each one is re-randomized by very slightly moving its handle
(randomization level is 0.1 throughout):
</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="shapes-f18.svg"/>
</imageobject>
</mediaobject>
</figure>

<para>
And here is the middle star from the previous row, with the randomization level varying
from -0.2 to 0.2:
</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="shapes-f19.svg"/>
</imageobject>
</mediaobject>
</figure>

<para>
<keycap>Alt+drag</keycap> a handle of the middle star in this row and observe as it
morphs into its neighbors on the right and left &#8212; and beyond.
</para>

<para>
You will probably find your own applications for randomized stars, but I am especially
fond of rounded amoeba-like blotches and large roughened planets with fantastic
landscapes:
</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="shapes-f20.svg"/>
</imageobject>
</mediaobject>
</figure>

</sect1>

<sect1>
<title>Spirals</title>
<para>
Inkscape's spiral is a versatile shape, and though not as immersing as the star,
it is sometimes very useful. A spiral, like a star, is drawn from the center; while
drawing as well as while editing,
</para>

<itemizedlist>
<listitem><para>
<keycap>Ctrl+drag</keycap> to snap angle to 15 degree increments.
</para></listitem>
</itemizedlist>

<para>
Once drawn, a spiral has two handles at its inner and outer ends.  Both handles, when
simply dragged, roll or unroll the spiral (i.e. “continue” it, changing the number of
turns). Other shortcuts:
</para>

<para>
Outer handle:
</para>

<itemizedlist>
<listitem><para>
<keycap>Shift+drag</keycap> to scale/rotate around center (no rolling/unrolling).
</para></listitem>
<listitem><para>
<keycap>Alt+drag</keycap> to lock radius while rolling/unrolling.
</para></listitem>
</itemizedlist>

<para>
Inner handle:
</para>

<itemizedlist>
<listitem><para>
<keycap>Alt+drag</keycap> vertically to converge/diverge.
</para></listitem>
<listitem><para>
<keycap>Alt+click</keycap> to reset divergence.
</para></listitem>
<listitem><para>
<keycap>Shift+click</keycap> to move the inner handle to the center.
</para></listitem>
</itemizedlist>

<para>
The <firstterm>divergence</firstterm> of a spiral is the measure of nonlinearity of its
winds. When it is equal to 1, the spiral is uniform; when it is less than 1
(<keycap>Alt+drag</keycap> upwards), the spiral is denser on the periphery; when it
is greater than 1 (<keycap>Alt+drag</keycap> downwards), the spiral is denser
towards the center:
</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="shapes-f21.svg"/>
</imageobject>
</mediaobject>
</figure>

<para>
The maximum number of spiral turns is 1024. 
</para>

<para>
Just as the Ellipse tool is good not only for ellipses but also for arcs (lines of
constant curvature), the Spiral tool is useful for making curves of <emphasis>smoothly
varying</emphasis> curvature. Compared to a plain Bezier curve, an arc or a spiral is
often more convenient because you can make it shorter or longer by dragging a handle
along the curve without affecting its shape. Also, while a spiral is normally drawn
without fill, you can add fill and remove stroke for interesting effects.
</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="shapes-f22.svg"/>
</imageobject>
</mediaobject>
</figure>

<para>
Especially interesting are spirals with dotted stroke &#8212; they combine the smooth
concentration of the shape with regular equispaced marks (dots or dashes) for beautiful
moire effects:
</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="shapes-f23.svg"/>
</imageobject>
</mediaobject>
</figure>

</sect1>

<sect1>
<title>Conclusion</title>
<para>
Inkscape's shape tools are very powerful. Learn their tricks and play with them at your
leisure &#8212; this will pay off when you do your design work, because using shapes
instead of simple paths often makes vector art faster to create and easier to modify. If
you have any ideas for further shape improvements, please contact the developers.
</para>
</sect1>

</chapter>
</book>
