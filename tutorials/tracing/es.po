msgid ""
msgstr ""
"Project-Id-Version: Inkscape tutorial: Tracing\n"
"POT-Creation-Date: 2018-02-15 21:53+0100\n"
"PO-Revision-Date: 2007-07-24 01:04+0200\n"
"Last-Translator: GLUD-ACL (Grupo Linux universidad Distrital - Academia y "
"Conocimiento Libre) <glud-acl@listas.udistrital.edu.co>\n"
"Language-Team: Spanish <es@li.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: tracing-f06.svg:46(format) tracing-f05.svg:46(format)
#: tracing-f04.svg:42(format) tracing-f03.svg:46(format)
#: tracing-f02.svg:46(format) tracing-f01.svg:46(format)
msgid "image/svg+xml"
msgstr "image/svg+xml"

#: tracing-f06.svg:68(tspan) tracing-f05.svg:68(tspan) tracing-f04.svg:64(tspan)
#: tracing-f03.svg:68(tspan) tracing-f02.svg:68(tspan)
#, no-wrap
msgid "Original Image"
msgstr ""

#: tracing-f06.svg:80(tspan)
#, no-wrap
msgid "Traced Image / Output Path - Simplified"
msgstr ""

#: tracing-f06.svg:84(tspan)
#, no-wrap
msgid "(384 nodes)"
msgstr ""

#: tracing-f05.svg:80(tspan)
#, no-wrap
msgid "Traced Image / Output Path"
msgstr ""

#: tracing-f05.svg:84(tspan)
#, no-wrap
msgid "(1,551 nodes)"
msgstr ""

#: tracing-f04.svg:76(tspan) tracing-f04.svg:92(tspan)
#, no-wrap
msgid "Quantization (12 colors)"
msgstr ""

#: tracing-f04.svg:80(tspan) tracing-f03.svg:84(tspan) tracing-f02.svg:84(tspan)
#, no-wrap
msgid "Fill, no Stroke"
msgstr ""

#: tracing-f04.svg:96(tspan) tracing-f03.svg:100(tspan)
#: tracing-f02.svg:99(tspan)
#, no-wrap
msgid "Stroke, no Fill"
msgstr ""

#: tracing-f03.svg:80(tspan) tracing-f03.svg:96(tspan)
#, fuzzy, no-wrap
msgid "Edge Detected"
msgstr "Detección de Bordes Óptima"

#: tracing-f02.svg:80(tspan) tracing-f02.svg:95(tspan)
#, no-wrap
msgid "Brightness Threshold"
msgstr "Luminosidad de la imágen"

#: tracing-f01.svg:68(tspan)
#, no-wrap
msgid "Main options within the Trace dialog"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-tracing.xml:51(None)
#, fuzzy
msgid "@@image: 'tracing-f01.svg'; md5=2c6fa4df268391fef79a8e38da09b4f2"
msgstr "@@image: 'tracing-f06.svg'; md5=5a14a98c0d02c23df7f79edc5284eda9"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-tracing.xml:76(None)
#, fuzzy
msgid "@@image: 'tracing-f02.svg'; md5=dee88e327cea56b8998bfc5bc47a9bca"
msgstr "@@image: 'tracing-f05.svg'; md5=e5f60b76390fe5b5807a36b43c710fc8"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-tracing.xml:99(None)
#, fuzzy
msgid "@@image: 'tracing-f03.svg'; md5=59f05c2216f52a9f965e95c86aa60ac4"
msgstr "@@image: 'tracing-f03.svg'; md5=29b200b20ad212e646e94496945bc9ac"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-tracing.xml:121(None)
#, fuzzy
msgid "@@image: 'tracing-f04.svg'; md5=b8d3e901eedee188cb88f891b71ad2e3"
msgstr "@@image: 'tracing-f05.svg'; md5=e5f60b76390fe5b5807a36b43c710fc8"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-tracing.xml:141(None)
#, fuzzy
msgid "@@image: 'tracing-f05.svg'; md5=108ce54ad9493c2d89c059e3e723fe32"
msgstr "@@image: 'tracing-f05.svg'; md5=e5f60b76390fe5b5807a36b43c710fc8"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-tracing.xml:153(None)
#, fuzzy
msgid "@@image: 'tracing-f06.svg'; md5=4616586fcb60d769fdd8c09ee1cb1780"
msgstr "@@image: 'tracing-f05.svg'; md5=e5f60b76390fe5b5807a36b43c710fc8"

#: tutorial-tracing.xml:4(title)
#, fuzzy
msgid "Tracing bitmaps"
msgstr "Vectorizar"

#: tutorial-tracing.xml:8(para)
msgid ""
"One of the features in Inkscape is a tool for tracing a bitmap image into a "
"&lt;path&gt; element for your SVG drawing. These short notes should help you "
"become acquainted with how it works."
msgstr ""
"Una de las funciones de Inkscape es una herramienta para vectorizado de "
"imágenes de mapas de bits en un &lt;trazo&gt; elemento para el dibujado de "
"SVG. Estas cortas notas le ayudarán a conocer como trabaja esto."

#: tutorial-tracing.xml:16(para)
msgid ""
"Currently Inkscape employs the Potrace bitmap tracing engine (<ulink url="
"\"http://potrace.sourceforge.net\">potrace.sourceforge.net</ulink>) by Peter "
"Selinger. In the future we expect to allow alternate tracing programs; for "
"now, however, this fine tool is more than sufficient for our needs."
msgstr ""
"Actualmente Inkscape emplea el motor de vectorizado de mapa de bits Potrace "
"(<ulink url=\"http://potrace.sourceforge.net\">potrace.sourceforge.net</"
"ulink>) por Peter Selinger. En el futuro, esperamos que permita alternar "
"programas de vectorizado; por ahora, sin embargo, esta fina herramienta es "
"más que suficiente para lo que necesita."

#: tutorial-tracing.xml:23(para)
msgid ""
"Keep in mind that the Tracer's purpose is not to reproduce an exact duplicate "
"of the original image; nor is it intended to produce a final product. No "
"autotracer can do that. What it does is give you a set of curves which you "
"can use as a resource for your drawing."
msgstr ""
"Tenga en mente que el propósito del Vectorizar no es reproducir un duplicado "
"exacto de la imágen original; o intentar producir un producto final. El "
"autotrazado no hace eso. Lo que hace es darle un set de curvas las cuales "
"usted puede emplear como una fuente de ayuda para sus dibujos."

#: tutorial-tracing.xml:30(para)
msgid ""
"Potrace interprets a black and white bitmap, and produces a set of curves. "
"For Potrace, we currently have three types of input filters to convert from "
"the raw image to something that Potrace can use."
msgstr ""
"Potrace interpreta mapas de bits blanco y negro y produce un set de curvas. "
"Para Potrace, actualmente poseemos tres tipos de filtros de salida, para "
"convertir desde imágenes brutas a algo que Potrace pueda usar."

#: tutorial-tracing.xml:36(para)
msgid ""
"Generally the more dark pixels in the intermediate bitmap, the more tracing "
"that Potrace will perform. As the amount of tracing increases, more CPU time "
"will be required, and the &lt;path&gt; element will become much larger. It is "
"suggested that the user experiment with lighter intermediate images first, "
"getting gradually darker to get the desired proportion and complexity of the "
"output path."
msgstr ""
"Generalmente los pixeles más oscuros en un mapa de bit intermedio, es el "
"mayor trazo que Potrace puede desarrollar. A mayor cantidad de trazos, más "
"tiempo la CPU requerira y el elemento &lt;trazo&gt; se convertirá en uno más "
"grande. Se sugiere que los usuarios experimenten primero con imágenes "
"intermédias clara, configurando gradualmente la opacidad para obener la "
"proporción y complejidad del trazo resultante."

#: tutorial-tracing.xml:44(para)
msgid ""
"To use the tracer, load or import an image, select it, and select the "
"<command>Path &gt; Trace Bitmap</command> item, or <keycap>Shift+Alt+B</"
"keycap>."
msgstr ""
"Para usar el vectorizado, cargue o importe una imágen, selecciónela, y "
"seleccione <command>Trazo &gt; Vectorizar un mapa de bits</command>, o "
"<keycap>Mayus+Alt+B</keycap>."

#: tutorial-tracing.xml:56(para)
msgid "The user will see the three filter options available:"
msgstr "El usuario observará las tres opciones de filtro disponibles:"

#: tutorial-tracing.xml:61(para)
#, fuzzy
msgid "Brightness Cutoff"
msgstr "Luminosidad de la imágen"

#: tutorial-tracing.xml:66(para)
msgid ""
"This merely uses the sum of the red, green and blue (or shades of gray) of a "
"pixel as an indicator of whether it should be considered black or white. The "
"threshold can be set from 0.0 (black) to 1.0 (white). The higher the "
"threshold setting, the fewer the number pixels that will be considered to be "
"“white”, and the intermediate image with become darker."
msgstr ""
"Esta usa realmente la suma del rojo, verde y azul (o escala de grices) de un "
"pixel como un indicador de si este puede ser considerado blanco o negro. La "
"luminosidad puede ser configurada desde 0.0 (negro) a 1.0 (blanco). La mayor "
"configuración del umbral, el menor número de pixeles que serán considerados "
"para ser \\u201cwhite\\u201d, y la imágen intermedia que se convertirá en "
"oscura."

#: tutorial-tracing.xml:82(para)
#, fuzzy
msgid "Edge Detection"
msgstr "Detección de Bordes Óptima"

#: tutorial-tracing.xml:87(para)
msgid ""
"This uses the edge detection algorithm devised by J. Canny as a way of "
"quickly finding isoclines of similar contrast. This will produce an "
"intermediate bitmap that will look less like the original image than does the "
"result of Brightness Threshold, but will likely provide curve information "
"that would otherwise be ignored. The threshold setting here (0.0 – 1.0) "
"adjusts the brightness threshold of whether a pixel adjacent to a contrast "
"edge will be included in the output. This setting can adjust the darkness or "
"thickness of the edge in the output."
msgstr ""
"Este filtro usa el arlgoritmo de detección de bordes desarrollado por J. "
"Canny, el cual es un modo de búsqueda rápida de isóclinas de contrastes "
"similares. Esto producirá un mapa de bits intermedio que será visto un poco "
"diferente a la imágen original que como lo hace la lumínusidad de la imágen, "
"pero provee la curva de información que de otra manera será ignorado. La "
"configuración del umbral es (0.0 \\u2013 1.0) ajustada por la luminosidad de "
"la imágen si es un pixel adjacente al borde del contasre que será incluido en "
"el resultado. Esta configuración puede ajustar la opacidad o grosor del borde "
"en el resultado."

#: tutorial-tracing.xml:105(para)
#, fuzzy
msgid "Color Quantization"
msgstr "REDUCCIÓN"

#: tutorial-tracing.xml:110(para)
msgid ""
"The result of this filter will produce an intermediate image that is very "
"different from the other two, but is very useful indeed. Instead of showing "
"isoclines of brightness or contrast, this will find edges where colors "
"change, even at equal brightness and contrast. The setting here, Number of "
"Colors, decides how many output colors there would be if the intermediate "
"bitmap were in color. It then decides black/white on whether the color has an "
"even or odd index."
msgstr ""
"El resultado de este filtro producirá una imágen intermedia que es muy "
"diferente de la otra segunda, pero es de hecho muy útil. En vez de mostrar "
"las isóclinas o brillo o contraste, esta buscará los bordes donde los colores "
"cambian, uniformemente igual a brillo y contraste. Las opciones de "
"configuración aquí son: Número de colores, decide cuantos colores de salida "
"pueden haber, si el mapa de bits intermedio era de color. Este entonces "
"decide blanco/negro según si el color ha sido uniforme o posee un indice raro."

#: tutorial-tracing.xml:126(para)
msgid ""
"The user should try all three filters, and observe the different types of "
"output for different types of input images. There will always be an image "
"where one works better than the others."
msgstr ""
"El usuario puede intentar todos los tres filtros y observar los diferentes "
"tipos de resultados para diferentes tipos de imágenes de entrada. Siempre "
"habrá una imágen donde uno trabajará mejor que otro."

#: tutorial-tracing.xml:132(para)
msgid ""
"After tracing, it is also suggested that the user try <command>Path &gt; "
"Simplify</command> (<keycap>Ctrl+L</keycap>) on the output path to reduce the "
"number of nodes. This can make the output of Potrace much easier to edit. For "
"example, here is a typical tracing of the Old Man Playing Guitar:"
msgstr ""
"Después del trazo, también se sugiere al usuario intentar <command>Trazo &gt; "
"Simplificar</command> (<keycap>Ctrl+L</keycap>) sobre el trazo resultante, "
"para reducir el número de nodos. Esto puede hacer el resultado del Potrace "
"mucho más simple de editar. Por ejemplo, aquí podemos observar el típico "
"trazo del Hombre Viejo Tocando Guitarra:"

#: tutorial-tracing.xml:146(para)
msgid ""
"Note the enormous number of nodes in the path. After hitting <keycap>Ctrl+L</"
"keycap>, this is a typical result:"
msgstr ""
"Note la gran cantidad de nodos en el trazo. Después de realizar <keycap>Ctrl"
"+L</keycap>, este es un resultado típico:"

#: tutorial-tracing.xml:158(para)
msgid ""
"The representation is a bit more approximate and rough, but the drawing is "
"much simpler and easier to edit. Keep in mind that what you want is not an "
"exact rendering of the image, but a set of curves that you can use in your "
"drawing."
msgstr ""
"La representación es un poco más aproximada y áspera, pero el dibujo es mucho "
"más simple y sencillo de editar. Mantenga en mente que lo que quiere no es "
"una réplica exacta de la imágen, pero un set de de curvas es lo que puede "
"usar en su dibujo."

#. Put one translator per line, in the form of NAME <EMAIL>, YEAR1, YEAR2
#: tutorial-tracing.xml:0(None)
msgid "translator-credits"
msgstr ""
"GLUD-ACL (Grupo Linux universidad Distrital - Academia y Conocimiento Libre) "
"<glud-acl@listas.udistrital.edu.co>, 2005."

#, fuzzy
#~ msgid "@@image: 'tracing-f01.svg'; md5=84a77c50eff0f7057e6610301d4ffffb"
#~ msgstr "@@image: 'tracing-f06.svg'; md5=5a14a98c0d02c23df7f79edc5284eda9"

#, fuzzy
#~ msgid "@@image: 'tracing-f02.svg'; md5=5ba65d7dacf78d58b5934590f697cf15"
#~ msgstr "@@image: 'tracing-f02.svg'; md5=51ff94b899765a6578c7599fd971a927"

#, fuzzy
#~ msgid "@@image: 'tracing-f03.svg'; md5=f387b58dffb42e02bf41f8d816ecd6ee"
#~ msgstr "@@image: 'tracing-f04.svg'; md5=b788b5eb50f2ff34f48817fcd14fae05"

#~ msgid "Optimal Edge Detection"
#~ msgstr "Detección de Bordes Óptima"

#, fuzzy
#~ msgid "@@image: 'tracing-f07.png'; md5=4d20bb3de466ef33adacd2bc02febe81"
#~ msgstr "@@image: 'tracing-f01.svg'; md5=518f33adc0040b2aaacbd0ae82bac226"
