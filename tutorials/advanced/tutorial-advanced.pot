msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2016-06-02 21:14+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: advanced-f12.svg:44(format) advanced-f06.svg:44(format) advanced-f09.svg:44(format) advanced-f02.svg:44(format) advanced-f08.svg:42(format) advanced-f05.svg:44(format) advanced-f01.svg:44(format) advanced-f16.svg:45(format) advanced-f11.svg:45(format) advanced-f13.svg:45(format) advanced-f14.svg:45(format) advanced-f07.svg:44(format) advanced-f15.svg:45(format) advanced-f03.svg:44(format) advanced-f10.svg:44(format) advanced-f04.svg:44(format)
msgid "image/svg+xml"
msgstr ""

#: advanced-f08.svg:63(tspan)
#, no-wrap
msgid "Original shapes"
msgstr ""

#: advanced-f08.svg:75(tspan)
#, no-wrap
msgid "Union (Ctrl++)"
msgstr ""

#: advanced-f08.svg:87(tspan)
#, no-wrap
msgid "Difference (Ctrl+-)"
msgstr ""

#: advanced-f08.svg:99(tspan)
#, no-wrap
msgid "Intersection"
msgstr ""

#: advanced-f08.svg:103(tspan)
#, no-wrap
msgid "(Ctrl+*)"
msgstr ""

#: advanced-f08.svg:115(tspan)
#, no-wrap
msgid "Exclusion"
msgstr ""

#: advanced-f08.svg:119(tspan)
#, no-wrap
msgid "(Ctrl+^)"
msgstr ""

#: advanced-f08.svg:131(tspan)
#, no-wrap
msgid "Division"
msgstr ""

#: advanced-f08.svg:135(tspan)
#, no-wrap
msgid "(Ctrl+/)"
msgstr ""

#: advanced-f08.svg:147(tspan)
#, no-wrap
msgid "Cut Path"
msgstr ""

#: advanced-f08.svg:151(tspan)
#, no-wrap
msgid "(Ctrl+Alt+/)"
msgstr ""

#: advanced-f08.svg:214(flowPara)
#, no-wrap
msgid "(bottom minus top)"
msgstr ""

#: advanced-f16.svg:65(tspan) advanced-f14.svg:86(tspan) advanced-f14.svg:94(tspan) advanced-f15.svg:76(tspan)
#, no-wrap
msgid "Inspiration"
msgstr ""

#: advanced-f13.svg:67(tspan) advanced-f14.svg:67(tspan)
#, no-wrap
msgid "Original"
msgstr ""

#: advanced-f13.svg:79(tspan)
#, no-wrap
msgid "Slight simplification"
msgstr ""

#: advanced-f13.svg:91(tspan)
#, no-wrap
msgid "Aggressive simplification"
msgstr ""

#: advanced-f14.svg:79(tspan)
#, no-wrap
msgid "Letter spacing decreased"
msgstr ""

#: advanced-f15.svg:67(tspan)
#, no-wrap
msgid "Letter spacing decreased, some letter pairs manually kerned"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-advanced.xml:77(None)
msgid "@@image: 'advanced-f01.svg'; md5=23eccfab3ab57eb3af4b319e46d7c6c0"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-advanced.xml:88(None)
msgid "@@image: 'advanced-f02.svg'; md5=3c987f800b29b629c7fadb7632eed0ab"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-advanced.xml:126(None)
msgid "@@image: 'advanced-f03.svg'; md5=7ec070b8d7fc72117a877489b5e432a5"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-advanced.xml:193(None)
msgid "@@image: 'advanced-f04.svg'; md5=da1a2bd2e84be77dac89d06b729a4d86"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-advanced.xml:220(None)
msgid "@@image: 'advanced-f05.svg'; md5=e826865cfa2d34da545244c20b4207d5"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-advanced.xml:245(None)
msgid "@@image: 'advanced-f06.svg'; md5=6fc95f912391d085bf4e5057e54e577b"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-advanced.xml:259(None)
msgid "@@image: 'advanced-f07.svg'; md5=7968c2c9f8e5c8805ae0c15b56220430"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-advanced.xml:274(None)
msgid "@@image: 'advanced-f08.svg'; md5=6a46406a6f26a61b75149cfefa565522"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-advanced.xml:311(None)
msgid "@@image: 'advanced-f09.svg'; md5=d34967d0d3e8f440d801dc9d2a0c3ea4"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-advanced.xml:327(None)
msgid "@@image: 'advanced-f10.svg'; md5=8153397ce8e149167289dcd36df24bf8"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-advanced.xml:356(None)
msgid "@@image: 'advanced-f11.svg'; md5=b5cadb91f7dbe96e734491c4f324ae6f"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-advanced.xml:378(None)
msgid "@@image: 'advanced-f12.svg'; md5=6045fdb18a2713ffdfd8fe587f485e43"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-advanced.xml:406(None)
msgid "@@image: 'advanced-f13.svg'; md5=668061d69eb0c9df28efe60aecd893cd"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-advanced.xml:450(None)
msgid "@@image: 'advanced-f14.svg'; md5=0aebc292254ad20519b222b8e0334750"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-advanced.xml:473(None)
msgid "@@image: 'advanced-f15.svg'; md5=dc0da53df2311bc52f7f08ea2cc8a971"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-advanced.xml:486(None)
msgid "@@image: 'advanced-f16.svg'; md5=d3c50eeaf2d4ff21a715834b2add4ab3"
msgstr ""

#: tutorial-advanced.xml:4(title)
msgid "Advanced"
msgstr ""

#: tutorial-advanced.xml:5(authors)
msgid "bulia byak, buliabyak@users.sf.net and josh andler, scislac@users.sf.net"
msgstr ""

#: tutorial-advanced.xml:9(para)
msgid "This tutorial covers copy/paste, node editing, freehand and bezier drawing, path manipulation, booleans, offsets, simplification, and text tool."
msgstr ""

#: tutorial-advanced.xml:13(para)
msgid "Use <keycap>Ctrl+arrows</keycap>, <keycap>mouse wheel</keycap>, or <keycap>middle button drag</keycap> to scroll the page down. For basics of object creation, selection, and transformation, see the Basic tutorial in <command>Help &gt; Tutorials</command>."
msgstr ""

#: tutorial-advanced.xml:21(title)
msgid "Pasting techniques"
msgstr ""

#: tutorial-advanced.xml:23(para)
msgid "After you copy some object(s) by <keycap>Ctrl+C</keycap> or cut by <keycap>Ctrl+X</keycap>, the regular <command>Paste</command> command (<keycap>Ctrl+V</keycap>) pastes the copied object(s) right under the mouse cursor or, if the cursor is outside the window, to the center of the document window. However, the object(s) in the clipboard still remember the original place from which they were copied, and you can paste back there by <command>Paste in Place</command> (<keycap>Ctrl+Alt+V</keycap>)."
msgstr ""

#: tutorial-advanced.xml:33(para)
msgid "Another command, <command>Paste Style</command> (<keycap>Shift+Ctrl+V</keycap>), applies the style of the (first) object on the clipboard to the current selection. The “style” thus pasted includes all the fill, stroke, and font settings, but not the shape, size, or parameters specific to a shape type, such as the number of tips of a star."
msgstr ""

#: tutorial-advanced.xml:40(para)
msgid "Yet another set of paste commands, <command>Paste Size</command>, scales the selection to match the desired size attribute of the clipboard object(s). There are a number of commands for pasting size and are as follows: Paste Size, Paste Width, Paste Height, Paste Size Separately, Paste Width Separately, and Paste Height Separately."
msgstr ""

#: tutorial-advanced.xml:47(para)
msgid "<command>Paste Size</command> scales the whole selection to match the overall size of the clipboard object(s). <command>Paste Width</command>/<command>Paste Height</command> scale the whole selection horizontally/vertically so that it matches the width/height of the clipboard object(s). These commands honor the scale ratio lock on the Selector Tool controls bar (between W and H fields), so that when that lock is pressed, the other dimension of the selected object is scaled in the same proportion; otherwise the other dimension is unchanged. The commands containing “Separately” work similarly to the above described commands, except that they scale each selected object separately to make it match the size/width/height of the clipboard object(s)."
msgstr ""

#: tutorial-advanced.xml:60(para)
msgid "Clipboard is system-wide - you can copy/paste objects between different Inkscape instances as well as between Inkscape and other applications (which must be able to handle SVG on the clipboard to use this)."
msgstr ""

#: tutorial-advanced.xml:68(title)
msgid "Drawing freehand and regular paths"
msgstr ""

#: tutorial-advanced.xml:70(para)
msgid "The easiest way to create an arbitrary shape is to draw it using the Pencil (freehand) tool (<keycap>F6</keycap>):"
msgstr ""

#: tutorial-advanced.xml:82(para)
msgid "If you want more regular shapes, use the Pen (Bezier) tool (<keycap>Shift+F6</keycap>):"
msgstr ""

#: tutorial-advanced.xml:93(para)
msgid "With the Pen tool, each <keycap>click</keycap> creates a sharp node without any curve handles, so a series of clicks produces a sequence of straight line segments. <keycap>click and drag</keycap> creates a smooth Bezier node with two collinear opposite handles. Press <keycap>Shift</keycap> while dragging out a handle to rotate only one handle and fix the other. As usual, <keycap>Ctrl</keycap> limits the direction of either the current line segment or the Bezier handles to 15 degree increments. Pressing <keycap>Enter</keycap> finalizes the line, <keycap>Esc</keycap> cancels it. To cancel only the last segment of an unfinished line, press <keycap>Backspace</keycap>."
msgstr ""

#: tutorial-advanced.xml:103(para)
msgid "In both freehand and bezier tools, the currently selected path displays small square <firstterm>anchors</firstterm> at both ends. These anchors allow you to <emphasis>continue</emphasis> this path (by drawing from one of the anchors) or <emphasis>close</emphasis> it (by drawing from one anchor to the other) instead of creating a new one."
msgstr ""

#: tutorial-advanced.xml:113(title)
msgid "Editing paths"
msgstr ""

#: tutorial-advanced.xml:115(para)
msgid "Unlike shapes created by shape tools, the Pen and Pencil tools create what is called <firstterm>paths</firstterm>. A path is a sequence of straight line segments and/or Bezier curves which, as any other Inkscape object, may have arbitrary fill and stroke properties. But unlike a shape, a path can be edited by freely dragging any of its nodes (not just predefined handles) or by directly dragging a segment of the path. Select this path and switch to the Node tool (<keycap>F2</keycap>):"
msgstr ""

#: tutorial-advanced.xml:131(para)
msgid "You will see a number of gray square <firstterm>nodes</firstterm> on the path. These nodes can be <firstterm>selected</firstterm> by <keycap>click</keycap>, <keycap>Shift+click</keycap>, or by <keycap>drag</keycap>ging a rubberband - exactly like objects are selected by the Selector tool. You can also click a path segment to automatically select the adjacent nodes. Selected nodes become highlighted and show their <firstterm>node handles</firstterm> - one or two small circles connected to each selected node by straight lines. The <keycap>!</keycap> key inverts node selection in the current subpath(s) (i.e. subpaths with at least one selected node); <keycap>Alt+!</keycap> inverts in the entire path."
msgstr ""

#: tutorial-advanced.xml:143(para)
msgid "Paths are edited by <keycap>drag</keycap>ging their nodes, node handles, or directly dragging a path segment. (Try to drag some nodes, handles, and path segments of the above path.) <keycap>Ctrl</keycap> works as usual to restrict movement and rotation. The <keycap>arrow</keycap> keys, <keycap>Tab</keycap>, <keycap>[</keycap>, <keycap>]</keycap>, <keycap>&lt;</keycap>, <keycap>&gt;</keycap> keys with their modifiers all work just as they do in selector, but apply to nodes instead of objects. You can add nodes anywhere on a path by either double clicking or by <keycap>Ctrl+Alt+click</keycap> at the desired location."
msgstr ""

#: tutorial-advanced.xml:152(para)
msgid "You can delete nodes with <keycap>Del</keycap> or <keycap>Ctrl+Alt+click</keycap>. When deleting nodes it will try to retain the shape of the path, if you desire for the handles of the adjacent nodes to be retracted (not retaining the shape) you can delete with <keycap>Ctrl+Del</keycap>. Additionally, you can duplicate (<keycap>Shift+D</keycap>) selected nodes. The path can be broken (<keycap>Shift+B</keycap>) at the selected nodes, or if you select two endnodes on one path, you can join them (<keycap>Shift+J</keycap>)."
msgstr ""

#: tutorial-advanced.xml:161(para)
msgid "A node can be made <firstterm>cusp</firstterm> (<keycap>Shift+C</keycap>), which means its two handles can move independently at any angle to each other; <firstterm>smooth</firstterm> (<keycap>Shift+S</keycap>), which means its handles are always on the same straight line (collinear); <firstterm>symmetric</firstterm> (<keycap>Shift+Y</keycap>), which is the same as smooth, but the handles also have the same length; and <firstterm>auto-smooth</firstterm> (<keycap>Shift+A</keycap>), a special node that automatically adjusts the handles of the node and surrounding auto-smooth nodes to maintain a smooth curve. When you switch the type of node, you can preserve the position of one of the two handles by hovering your mouse over it, so that only the other handle is rotated/scaled to match."
msgstr ""

#: tutorial-advanced.xml:174(para)
msgid "Also, you can <firstterm>retract</firstterm> a node's handle altogether by <keycap>Ctrl+click</keycap>ing on it. If two adjacent nodes have their handles retracted, the path segment between them is a straight line. To pull out the retracted node, <keycap>Shift+drag</keycap> away from the node."
msgstr ""

#: tutorial-advanced.xml:183(title)
msgid "Subpaths and combining"
msgstr ""

#: tutorial-advanced.xml:184(para)
msgid "A path object may contain more than one <firstterm>subpath</firstterm>. A subpath is a sequence of nodes connected to each other. (Therefore, if a path has more than one subpath, not all of its nodes are connected.) Below left, three subpaths belong to a single compound path; the same three subpaths on the right are independent path objects:"
msgstr ""

#: tutorial-advanced.xml:198(para)
msgid "Note that a compound path is not the same as a group. It's a single object which is only selectable as a whole. If you select the left object above and switch to node tool, you will see nodes displayed on all three subpaths. On the right, you can only node-edit one path at a time."
msgstr ""

#: tutorial-advanced.xml:205(para)
msgid "Inkscape can <command>Combine</command> paths into a compound path (<keycap>Ctrl+K</keycap>) and <command>Break Apart</command> a compound path into separate paths (<keycap>Shift+Ctrl+K</keycap>). Try these commands on the above examples. Since an object can only have one fill and stroke, a new compound path gets the style of the first (lowest in z-order) object being combined."
msgstr ""

#: tutorial-advanced.xml:213(para)
msgid "When you combine overlapping paths with fill, usually the fill will disappear in the areas where the paths overlap:"
msgstr ""

#: tutorial-advanced.xml:225(para)
msgid "This is the easiest way to create objects with holes in them. For more powerful path commands, see “Boolean operations” below."
msgstr ""

#: tutorial-advanced.xml:232(title)
msgid "Converting to path"
msgstr ""

#: tutorial-advanced.xml:234(para)
msgid "Any shape or text object can be <firstterm>converted to path</firstterm> (<keycap>Shift+Ctrl+C</keycap>). This operation does not change the appearance of the object but removes all capabilities specific to its type (e.g. you can't round the corners of a rectangle or edit the text anymore); instead, you can now edit its nodes. Here are two stars - the left one is kept a shape and the right one is converted to path. Switch to node tool and compare their editability when selected:"
msgstr ""

#: tutorial-advanced.xml:250(para)
msgid "Moreover, you can convert to a path (“outline”) the <firstterm>stroke</firstterm> of any object. Below, the first object is the original path (no fill, black stroke), while the second one is the result of the <commands>Stroke to Path</commands> command (black fill, no stroke):"
msgstr ""

#: tutorial-advanced.xml:266(title)
msgid "Boolean operations"
msgstr ""

#: tutorial-advanced.xml:267(para)
msgid "The commands in the Path menu let you combine two or more objects using <firstterm>boolean operations</firstterm>:"
msgstr ""

#: tutorial-advanced.xml:279(para)
msgid "The keyboard shortcuts for these commands allude to the arithmetic analogs of the boolean operations (union is addition, difference is subtraction, etc.). The <command>Difference</command> and <command>Exclusion</command> commands can only apply to two selected objects; others may process any number of objects at once. The result always receives the style of the bottom object."
msgstr ""

#: tutorial-advanced.xml:287(para)
msgid "The result of the <command>Exclusion</command> command looks similar to <command>Combine</command> (see above), but it is different in that <command>Exclusion</command> adds extra nodes where the original paths intersect. The difference between <command>Division</command> and <command>Cut Path</command> is that the former cuts the entire bottom object by the path of the top object, while the latter only cuts the bottom object's stroke and removes any fill (this is convenient for cutting fill-less strokes into pieces)."
msgstr ""

#: tutorial-advanced.xml:299(title)
msgid "Inset and outset"
msgstr ""

#: tutorial-advanced.xml:300(para)
msgid "Inkscape can expand and contract shapes not only by scaling, but also by <firstterm>offsetting</firstterm> an object's path, i.e. by displacing it perpendicular to the path in each point. The corresponding commands are called <command>Inset</command> (<keycap>Ctrl+(</keycap>) and <command>Outset</command> (<keycap>Ctrl+)</keycap>). Shown below is the original path (red) and a number of paths inset or outset from that original:"
msgstr ""

#: tutorial-advanced.xml:316(para)
msgid "The plain <command>Inset</command> and <command>Outset</command> commands produce paths (converting the original object to path if it's not a path yet). Often, more convenient is the <command>Dynamic Offset</command> (<keycap>Ctrl+J</keycap>) which creates an object with a draggable handle (similar to a shape's handle) controlling the offset distance. Select the object below, switch to the node tool, and drag its handle to get an idea:"
msgstr ""

#: tutorial-advanced.xml:332(para)
msgid "Such a <firstterm>dynamic offset object</firstterm> remembers the original path, so it does not “degrade” when you change the offset distance again and again. When you don't need it to be adjustable anymore, you can always convert an offset object back to path."
msgstr ""

#: tutorial-advanced.xml:338(para)
msgid "Still more convenient is a <firstterm>linked offset</firstterm>, which is similar to the dynamic variety but is connected to another path which remains editable. You can have any number of linked offsets for one source path. Below, the source path is red, one offset linked to it has black stroke and no fill, the other has black fill and no stroke."
msgstr ""

#: tutorial-advanced.xml:346(para)
msgid "Select the red object and node-edit it; watch how both linked offsets respond. Now select any of the offsets and drag its handle to adjust the offset radius. Finally, note how you can move or transform the offset objects independently without losing their connection with the source."
msgstr ""

#: tutorial-advanced.xml:364(title)
msgid "Simplification"
msgstr ""

#: tutorial-advanced.xml:366(para)
msgid "The main use of the <command>Simplify</command> command (<keycap>Ctrl+L</keycap>) is reducing the number of nodes on a path while <emphasis>almost</emphasis> preserving its shape. This may be useful for paths created by the Pencil tool, since that tool sometimes creates more nodes than necessary. Below, the left shape is as created by the freehand tool, and the right one is a copy that was simplified. The original path has 28 nodes, while the simplified one has 17 (which means it is much easier to work with in node tool) and is smoother."
msgstr ""

#: tutorial-advanced.xml:383(para)
msgid "The amount of simplification (called the <firstterm>threshold</firstterm>) depends on the size of the selection. Therefore, if you select a path along with some larger object, it will be simplified more aggressively than if you select that path alone. Moreover, the <command>Simplify</command> command is <firstterm>accelerated</firstterm>. This means that if you press <keycap>Ctrl+L</keycap> several times in quick succession (so that the calls are within 0.5 sec from each other), the threshold is increased on each call. (If you do another Simplify after a pause, the threshold is back to its default value.) By making use of the acceleration, it is easy to apply the exact amount of simplification you need for each case."
msgstr ""

#: tutorial-advanced.xml:395(para)
msgid "Besides smoothing freehand strokes, <command>Simplify</command> can be used for various creative effects. Often, a shape which is rigid and geometric benefits from some amount of simplification that creates cool life-like generalizations of the original form - melting sharp corners and introducing very natural distortions, sometimes stylish and sometimes plain funny. Here's an example of a clipart shape that looks much nicer after <command>Simplify</command>:"
msgstr ""

#: tutorial-advanced.xml:413(title)
msgid "Creating text"
msgstr ""

#: tutorial-advanced.xml:415(para)
msgid "Inkscape is capable of creating long and complex texts. However, it's also pretty convenient for creating small text objects such as heading, banners, logos, diagram labels and captions, etc. This section is a very basic introduction into Inkscape's text capabilities."
msgstr ""

#: tutorial-advanced.xml:422(para)
msgid "Creating a text object is as simple as switching to the Text tool (<keycap>F8</keycap>), clicking somewhere in the document, and typing your text. To change font family, style, size, and alignment, open the Text and Font dialog (<keycap>Shift+Ctrl+T</keycap>). That dialog also has a text entry tab where you can edit the selected text object - in some situations, it may be more convenient than editing it right on the canvas (in particular, that tab supports as-you-type spell checking)."
msgstr ""

#: tutorial-advanced.xml:431(para)
msgid "Like other tools, Text tool can select objects of its own type - text objects -so you can click to select and position the cursor in any existing text object (such as this paragraph)."
msgstr ""

#: tutorial-advanced.xml:437(para)
msgid "One of the most common operations in text design is adjusting spacing between letters and lines. As always, Inkscape provides keyboard shortcuts for this. When you are editing text, the <keycap>Alt+&lt;</keycap> and <keycap>Alt+&gt;</keycap> keys change the <firstterm>letter spacing</firstterm> in the current line of a text object, so that the total length of the line changes by 1 pixel at the current zoom (compare to Selector tool where the same keys do pixel-sized object scaling). As a rule, if the font size in a text object is larger than the default, it will likely benefit from squeezing letters a bit tighter than the default. Here's an example:"
msgstr ""

#: tutorial-advanced.xml:455(para)
msgid "The tightened variant looks a bit better as a heading, but it's still not perfect: the distances between letters are not uniform, for example the “a” and “t” are too far apart while “t” and “i” are too close. The amount of such bad kerns (especially visible in large font sizes) is greater in low quality fonts than in high quality ones; however, in any text string and in any font you will probably find pairs of letters that will benefit from kerning adjustments."
msgstr ""

#: tutorial-advanced.xml:464(para)
msgid "Inkscape makes these adjustments really easy. Just move your text editing cursor between the offending characters and use <keycap>Alt+arrows</keycap> to move the letters right of the cursor. Here is the same heading again, this time with manual adjustments for visually uniform letter positioning:"
msgstr ""

#: tutorial-advanced.xml:478(para)
msgid "In addition to shifting letters horizontally by <keycap>Alt+Left</keycap> or <keycap>Alt+Right</keycap>, you can also move them vertically by using <keycap>Alt+Up</keycap> or <keycap>Alt+Down</keycap>:"
msgstr ""

#: tutorial-advanced.xml:491(para)
msgid "Of course you could just convert your text to path (<keycap>Shift+Ctrl+C</keycap>) and move the letters as regular path objects. However, it is much more convenient to keep text as text - it remains editable, you can try different fonts without removing the kerns and spacing, and it takes much less space in the saved file. The only disadvantage to the “text as text” approach is that you need to have the original font installed on any system where you want to open that SVG document."
msgstr ""

#: tutorial-advanced.xml:500(para)
msgid "Similar to letter spacing, you can also adjust <firstterm>line spacing</firstterm> in multi-line text objects. Try the <keycap>Ctrl+Alt+&lt;</keycap> and <keycap>Ctrl+Alt+&gt;</keycap> keys on any paragraph in this tutorial to space it in or out so that the overall height of the text object changes by 1 pixel at the current zoom. As in Selector, pressing <keycap>Shift</keycap> with any spacing or kerning shortcut produces 10 times greater effect than without Shift."
msgstr ""

#: tutorial-advanced.xml:511(title)
msgid "XML editor"
msgstr ""

#: tutorial-advanced.xml:513(para)
msgid "The ultimate power tool of Inkscape is the XML editor (<keycap>Shift+Ctrl+X</keycap>). It displays the entire XML tree of the document, always reflecting its current state. You can edit your drawing and watch the corresponding changes in the XML tree. Moreover, you can edit any text, element, or attribute nodes in the XML editor and see the result on your canvas. This is the best tool imaginable for learning SVG interactively, and it allows you to do tricks that would be impossible with regular editing tools."
msgstr ""

#: tutorial-advanced.xml:525(title)
msgid "Conclusion"
msgstr ""

#: tutorial-advanced.xml:527(para)
msgid "This tutorial shows only a small part of all capabilities of Inkscape. We hope you enjoyed it. Don't be afraid to experiment and share what you create. Please visit <ulink url=\"http://www.inkscape.org\">www.inkscape.org</ulink> for more information, latest versions, and help from user and developer communities."
msgstr ""

#. Put one translator per line, in the form of NAME <EMAIL>, YEAR1, YEAR2
#: tutorial-advanced.xml:0(None)
msgid "translator-credits"
msgstr ""

