# Makefile written by 
# Nicolas Dufour <nicoduf@yahoo.fr>, 2010.
# Based on the tutorials' Makefile.target file.

# some tool definitions
XML2PO       := xml2po

# the current path (which will be inside a tutorial directory) 
# with the slashes replaced by spaces
PATHELEMS := $(subst /, ,$(PWD))

# the existing po files are used to determine which HTML and SVG files to build
SRC := $(wildcard *.po)

# all languages for which a po file has been found
MYLANGS := $(SRC:.po=)

# generate filenames for HTML keys
# Example: "keys. + de.html" --> keys.de.html
TARGETS_HTML := $(addprefix keys., $(SRC:.po=.html))

.PHONY: all
all: html

.PHONY: html
html: $(TARGETS_HTML)
	mv keys.en.html keys.html 

keys.%.html: keys.%.xml #  keys-html.xsl
	xsltproc keys-html.xsl keys.$*.xml > keys.$*.html

# generate the localized XML version of the keys reference:
keys.%.xml: %.po keys.xml
	$(XML2PO) --po-file=$< keys.xml > $@
	sed -e "s/\($(TUTORIAL)-f[0-9][0-9]\).\([a-z][a-z][a-z]\)/\1-$(subst .po,,$<).\2/" $@ > $@.temp.xml
	mv $@.temp.xml $@

.PHONY: update-po
update-po: keys.pot check_mylang
	msgmerge $(MYLANG).po keys.pot > updated.$(MYLANG).po
	mv updated.$(MYLANG).po $(MYLANG).po

# keys-%.xml has been updated and you want the new stuff in your %.po:
%.po: keys.xml
	$(XML2PO) --update-translation=$@ keys.xml
	msgcat --width=80 --output-file=$@ $@

# use this at the very beginning, when there is no translation at all
.PHONY: pot
pot: keys.pot

keys.pot: keys.xml
	$(XML2PO) -k --output=$@ $^

# use this when there is a *.%.xml, but you want a %.po:
#%.po: tutorial-$(TUTORIAL).%.xml
#	$(XML2PO) --output=$@ --reuse=$* tutorial-$(TUTORIAL).xml


# This target is not as effective as it could be since xml2po cleverly "repairs"
# certain wrong markup
.PHONY: check
check: keys.$(MYLANG).xml $(MYLANG).po check_mylang
	xmllint --valid --noout keys.$(MYLANG).xml

.PHONY: copy
copy: copy-html

.PHONY: copy-html
copy-html:
	mkdir -p ../export-website/keys
	for file in *.html; do cp "$$file" "../export-website/keys/keys$(INKVERSION)$${file#keys}"; done
	cp keys.css ../export-website/keys

.PHONY: clean
clean:
	rm -f *.html

.PHONY: help
help:
	@echo "Targets:"
	@echo "   all INKVERSION=<VERSION>   - Create HTML files for Inkscape version <VERSION> (091, 092...)"
	@echo "   html INKVERSION=<VERSION>  - Create HTML files for Inkscape version <VERSION>"
	@echo "   pot                        - Create a keys.pot file for translators (who will copy it to <LANG>.po)"
	@echo "   update-po MYLANG=<LANG>    - Update all po files for language <LANG>"
	@echo "   copy                       - Copy generated HTML key files to the export directory"
	@echo "   clean                      - Remove all generated HTML key files"


.PHONY: check_mylang
check_mylang:
ifndef MYLANG
	@echo "E:Syntax error - you didn't define MYLANG."
	@echo ""
	@$(MAKE) -s help
	@exit 1
else
	@if [ ! -r $(MYLANG).po ]; then \
	echo "E:Language file '$(MYLANG).po' does not exist!"; \
	echo ""; \
	exit 1; \
	fi
endif

# TODO: check pofilter from translate-toolkit
# TODO: make update-po work on all languages
